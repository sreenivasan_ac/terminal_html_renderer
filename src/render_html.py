import click
# from html.parser import HTMLParser
from xml.dom.minidom import parse, parseString
from helper import *
from sys import stderr, stdout

import cssutils

def getText(nodelist):
    rc = []
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
            rc.append(node.data)
    return ''.join(rc)

def render_para_element(node, node_to_style_rules_map):
    # {'color': 'blue', 'font-weight': 'bold'}
    # print(style.YELLOW("Hello, ") + style.RESET("World!"))

    output = getText(node.childNodes)

    if 'color' in node_to_style_rules_map[node]:
        if node_to_style_rules_map[node]['color'] == 'blue':
            output = style.BLUE(output)
        if node_to_style_rules_map[node]['color'] == 'red':
            output = style.RED(output)
    if 'font-weight' in node_to_style_rules_map[node]:
        if node_to_style_rules_map[node]['color'] == 'bold':
            output = style.BOLD(output)
    output += style.RESET("")
    print(output)


def render_element(node, node_to_style_rules_map):
    if node.tagName == "table":
        render_table_element(node)
    elif node.tagName == "p":
        render_para_element(node, node_to_style_rules_map)
    elif node.tagName == "div":
        render_element(node.childNodes[0], node_to_style_rules_map)

def render_dom(dom, node_to_style_rules_map):
    showHtmlTitle(dom.getElementsByTagName("title"))
    body = dom.getElementsByTagName("body")[0]
    for node in body.childNodes:
        render_element(node, node_to_style_rules_map)

def renderNode(node):
    print(getText(node))

def showHtmlTitle(title):
    if title:
        renderNode(title[0].childNodes)

def updateStyle(rule, style_mapping):
    rules = rule.style.cssText.split(";\n")
    for rule_ in rules:
        style_key, style_value = rule_.split(": ")
        style_mapping[style_key] = style_value

    return style_mapping

def rule_applies_to_this_node(node, rule, style_selector_classes, ancestors, class_to_node_mapping):
    current_node_class = node.getAttribute("class")
    if current_node_class != style_selector_classes[-1]:
        return False

    if len(style_selector_classes) == 1:
        # Check this line if it is correct
        if style_selector_classes[0] in class_to_node_mapping and node in class_to_node_mapping[style_selector_classes[0]]:
            return True

    if ">" in style_selector_classes:
        parentClass = style_selector_classes[0]
        childClass = style_selector_classes[-1]
        if childClass in class_to_node_mapping and node.parentNode in class_to_node_mapping[parentClass]:
            return True
        else:
            return False

    # style_selector_classes = []
    # ancestors_class_dom = []

    ancestors_class_dom = []
    for ancestor in ancestors:
        class_ = ancestor.getAttribute("class")
        if class_:
            ancestors_class_dom.append(class_)


    i = 0
    j = 0
    ancestors_class_dom.append(current_node_class)
    while(j < len(style_selector_classes) and i < len(ancestors_class_dom)):
        if style_selector_classes[j] == ancestors_class_dom[i]:
            j += 1
        i += 1

    if j == len(style_selector_classes):
        return True
    else:
        return False

def parseCSS(dom, css, ancestors, class_to_node_mapping):
    sheet = cssutils.parseString(css)
    node_to_style_rules_map = {}
    for rule in sheet:
        style_selector_classes = rule.selectorText.split(" ")
        style_selector_classes = [class_.replace(".", "") for class_ in style_selector_classes]

        class_ = style_selector_classes[-1]

        if class_ not in class_to_node_mapping:
            class_to_node_mapping[class_] = []

        for node in class_to_node_mapping[class_]:
            if node not in node_to_style_rules_map:
                node_to_style_rules_map[node] = {}

            if rule_applies_to_this_node(node, rule, style_selector_classes, ancestors[node], class_to_node_mapping):
                node_to_style_rules_map[node] = updateStyle(rule, node_to_style_rules_map[node])

    return node_to_style_rules_map

def populate_ancestors_class_mapping(dom):
    stack, visited = [], []
    stack.append(dom.childNodes[0])
    ancestors = {}
    class_to_node_mapping = {}
    ancestors[dom.childNodes[0]] = []

    while(stack):
        curr = stack.pop()
        if curr in visited:
            continue

        class_ = ''
        if curr.nodeType != curr.TEXT_NODE:
            class_ = curr.getAttribute("class")
        if class_:
            classes = class_.split(" ")
            for class_ in classes:
                if class_ in class_to_node_mapping:
                    class_to_node_mapping[class_].append(curr)
                else:
                    class_to_node_mapping[class_] = [curr]

        for child in curr.childNodes:
            stack.append(child)
            ancestors[child] = ancestors[curr] + [curr]


    # TODO only classes maybe required and Not the object pointers
    return ancestors, class_to_node_mapping

def render_table_element(table):
    # Validation check if number of columns in each rows are equal
    # This does Not support column SPAN property yet
    min_cols = min((len(tr.childNodes) for tr in table.childNodes))
    max_cols = max((len(tr.childNodes) for tr in table.childNodes))
    if min_cols != max_cols:
        stderr.write("Table is out of shape, please make sure all columns have the same length.")
        stderr.flush()
        return

    # Populating Table Matrix using DOM Table Element
    table_list = []
    for tr_row in table.childNodes:
         table_list.append((list(getText(td.childNodes) for td in tr_row.childNodes)))


    additional_spacing = 1

    heading_separator = '| '
    horizontal_split = '| '

    rc_separator = ''

    key_list = []
    firstRow = table.firstChild
    # if firstRow.firstChild.tagName == "th":
    # We are converting Table DOM elements to 2D Grid
    key_list = list(getText(td.childNodes) for td in firstRow.childNodes)

    rc_len_values = []
    table_ = {}
    full_row = False

    for index, key in enumerate(key_list):
        table_[key] = list(table_list[v][index] for v in range(1, len(table_list)))

    # Calculating how much spaces we have to leave for printing the Table
    for index, key in enumerate(key_list):

        rc_len = len(max((v for v in table_[key]), key=lambda q: len(str(q))))
        rc_len_values += ([rc_len, [key]] for n in range(len(table_[key])))

        heading_line = (key + (" " * (rc_len + (additional_spacing + 1)))) + heading_separator
        stdout.write(heading_line)

        rc_separator += ("-" * (len(key) + (rc_len + (additional_spacing + 1)))) + '+-'

        if key is key_list[-1]:
            stdout.flush()
            stdout.write('\n' + rc_separator + '\n')

    value_list = [v for vl in table_.values() for v in vl]

    aligned_data_offset = max_cols

    row_count = len(key_list)

    next_idx = 0
    newline_indicator = 0
    iterations = 0

    for n in range(len(value_list)):
        key = rc_len_values[next_idx][1][0]
        rc_len = rc_len_values[next_idx][0]

        line = ('{:{}} ' + " " * len(key)).format(value_list[next_idx], str(rc_len + additional_spacing)) + horizontal_split

        if next_idx >= (len(value_list) - aligned_data_offset):
            next_idx = iterations + 1
            iterations += 1
        else:
            next_idx += aligned_data_offset

        if newline_indicator >= row_count:
            if full_row:
                stdout.flush()
                stdout.write('\n' + rc_separator + '\n')
            else:
                stdout.flush()
                stdout.write('\n')

            newline_indicator = 0

        stdout.write(line)
        newline_indicator += 1

    stdout.write('\n' + rc_separator + '\n')
    stdout.flush()



@click.command()
@click.option('--html_filename', default='input1.html', help='HTML Input')
@click.option('--css_filename', default='input1.css', help='CSS File Input')

def render(html_filename, css_filename):
    """Program that simulates a simple Browser to render the HTML and CSS in Terminal"""
    # print(html_filename, css_filename)

    html = read_file(html_filename)

    dom = parseString(html)

    ancestors, class_to_node_mapping = populate_ancestors_class_mapping(dom)

    css = read_file(css_filename)
    node_to_style_rules_map = parseCSS(dom, css, ancestors, class_to_node_mapping)
    render_dom(dom, node_to_style_rules_map)

    # ".[s]* [>]+ .[s]* [{]* [[s]: [s]]* }".match()
    # https://stackoverflow.com/questions/236979/parsing-css-by-regex

if __name__ == '__main__':
    render()
