import os

class style():
    BLACK = lambda x: '\033[30m' + str(x)
    RED = lambda x: '\033[31m' + str(x)
    GREEN = lambda x: '\033[32m' + str(x)
    YELLOW = lambda x: '\033[33m' + str(x)
    BLUE = lambda x: '\033[34m' + str(x)
    MAGENTA = lambda x: '\033[35m' + str(x)
    CYAN = lambda x: '\033[36m' + str(x)
    WHITE = lambda x: '\033[37m' + str(x)
    BOLD = lambda x: '\033[1m' + str(x)
    RESET = lambda x: '\033[0m' + str(x)
    UNDERLINE = lambda x: '\033[4m' + str(x)
    END = lambda x: '\033[0m' + str(x)


def get_file_path(filename):
    script_dir = os.path.dirname(__file__)
    file_path = os.path.join(script_dir, './' + filename)
    return file_path

def read_file(filename):
    text = ""
    with open(get_file_path(filename)) as f:
        text = "".join([x.strip() for x in f])
    return text
