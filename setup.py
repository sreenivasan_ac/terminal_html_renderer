from setuptools import setup, find_packages

exec(open('src/version.py').read())

with open("README.md", "r") as readme_file:
    readme = readme_file.read()

setup(name='terminal_html_renderer',
      version=__version__,
      description='Terminal HTML Renderer',
      long_description=readme,
      long_description_content_type="text/markdown",
      url='https://github.com/sreenivasanac/terminal_html_renderer',
      author='Sreenivasan AC',
      author_email='sreenivasan.nitt@gmail.com',
      license='EULA',
      packages=find_packages(),
      zip_safe=False)
