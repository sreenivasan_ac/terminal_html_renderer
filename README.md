# Terminal HTML Renderer

Terminal HTML Renderer

## Approach

BeautifulSoup and lxml could have been used, but since they are external packages I have Not used them. I have tried python built-in packages like [xml.dom.minidom](https://docs.python.org/3/library/xml.dom.minidom.html) module, and [html.parser](https://docs.python.org/3.2/library/html.parser.html) and would be more from ground-up for this assignment.

For parsing HTML into DOM, since [html.parser](https://docs.python.org/3.2/library/html.parser.html) is Event-driven, hence I have preferred [xml.dom.minidom](https://docs.python.org/3/library/xml.dom.minidom.html)

I implemented this in around 6-8 Hours, from ideation to implementation and testing.
For lack of time  I will explain How to do the CSS part.
The CSS has to be parsed, and iterated, and the DOM object Nodes should be queried (getElementByID or getElementByTagName) and then their width should be updated, and when we are making the calculations, we have to update

For looking at my coding style and pattern, you can take a look at another Python Module I had created for Wordification of Phone Numbers
[https://github.com/sreenivasanac/vanitynumber/](https://github.com/sreenivasanac/vanitynumber/)
That is finding Wordification of Toll free number something like
```
+1-1800-483-8279 == 1800-FUN-TIME
```

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

## Assumptions Made

* HTML should Have a Body Tag
* It should be a Valid HTML
* Table should have atleast one row
* Para element can Not come INSIDE Table, it can come outside the Table, any number of times, as a child of Body
* Body can have a Title
* Table is currently LEFT Aligned, if required , it can be taken as an input, and then the spacing can be adjusted
* Currently CSS width, etc are Not used, it can be added in the calculation when allocating spaces in the table, by taking MAX of given width , OR the width of the text

### Installing

```
virtualenv -p python3 venv

git clone https://github.com/sreenivasanac/terminal_html_renderer
cd terminal_html_renderer/
pip install -r requirements.txt
python setup.py install
```

## Demo

```
python src/render_html.py --html_filename=input1.html --css_filename=input1.css
```
```
Heading      | Heading2      |
-------------+---------------+-
Num1         | Num2          |
Num3         | Num4          |
-------------+---------------+-
```


```
python src/render_html.py --html_filename=input2.html --css_filename=input2.css
```

```
Name         | Mark1     | Mark2    |
-------------+-----------+----------+-
Arjun        | 100       | 90       |
Krishna      | 90        | 82       |
Kishore      | 72        | 64       |
-------------+-----------+----------+-
```
## Running the tests

```
pytest
```

## Built With

* [click](https://click.palletsprojects.com/en/7.x/) - Python package for creating command line interfaces
* [xml.dom.minidom](https://docs.python.org/3/library/xml.dom.minidom.html) - Minimal DOM implementation
* [cssutils](https://pythonhosted.org/cssutils) CSS Parsing library

## Authors

* [**Sreenivasan AC**](https://www.linkedin.com/in/sreenivasan-ac/)

## License

This project is licensed under EULA Restrictive License - see the [LICENSE](LICENSE) file for details
